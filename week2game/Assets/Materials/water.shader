﻿Shader "Unlit/water"
{
	Properties
	{
		[Header(Shaderin hommat)]
		[Space]
		_Color ("Color", Color) = (0, .2, .8, .5)
		_MainTex ("Texture", 2D) = "white" {}
		_Noise ("Noise map", 2D) = "white" {}
		_Speed ("Wave speed", Range(0 , 1)) = .5
		_Amount ("Wave amount", Range(0 , 1)) = .5
		_Height ("Wave height", Range(0 , 1)) = .5
		_Foam("Foamline Thickness", Range(0,3)) = 0.5
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" "Queue"="Transparent" }
		LOD 100
		Blend SrcAlpha OneMinusSrcAlpha

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			//#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				//UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
				float4 scrPos : TEXCOORD1;
			};

			sampler2D _MainTex, _Noise;
			uniform sampler2D _CameraDepthTexture;
			float4 _MainTex_ST;
			fixed4 _Color;
			float _Speed, _Height, _Amount, _Foam;
			
			v2f vert (appdata v)
			{
				v2f o;
				float4 tex = tex2Dlod(_Noise, float4(v.uv.xy, 0, 0));
				v.vertex.y += sin(_Time.z * _Speed + (v.vertex.x * v.vertex.z * _Amount * tex)) * _Height;

				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.scrPos = ComputeScreenPos(o.vertex);
				//UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				fixed4 col = tex2D(_MainTex, i.uv) * _Color;
				
				
				half depth = LinearEyeDepth(SAMPLE_DEPTH_TEXTURE_PROJ(_CameraDepthTexture, UNITY_PROJ_COORD(i.scrPos))); // depth
                half4 foamLine =1 - saturate(_Foam * (depth - i.scrPos.w));// foam line by comparing depth and screenposition
                col += foamLine * _Color; // add the foam line and tint to the texture
				
				
				// apply fog
				//UNITY_APPLY_FOG(i.fogCoord, col);
				return col;
			}
			ENDCG
		}
	}
}
