﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterOffset : MonoBehaviour {
    public float speed = .5f;
    float offset;
    Renderer rend;
    public bool _FlipDirection;

	// Use this for initialization
	void Start () {
        rend = GetComponent<Renderer>();
	}
	
	// Update is called once per frame
	void Update () {
        offset += speed * Time.deltaTime;
        if (_FlipDirection)
        {
            rend.material.SetTextureOffset("_MainTex", new Vector2(offset, 0));
        }
        else {
            rend.material.SetTextureOffset("_MainTex", new Vector2(-offset, 0));
        }
            
	}
}
