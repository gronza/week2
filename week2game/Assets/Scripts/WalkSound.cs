﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class WalkSound : MonoBehaviour {

 public AudioClip sand;    
     
     void Start ()   
     {
         GetComponent<AudioSource> ().playOnAwake = false;
         GetComponent<AudioSource> ().clip = sand;
     }        
 
     void OnCollisionEnter (Collision collider) {
          if (collider.gameObject.CompareTag("sand")) {
             GetComponent<AudioSource> ().Play ();
          }
     
         
 }
 }
