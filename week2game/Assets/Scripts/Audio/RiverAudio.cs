﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RiverAudio : MonoBehaviour {
    public GameObject river;
    //Mesh riverMesh;
    public GameObject player;
    Collider riverBounds;
   // Bounds riverBounds;

    AudioSource audioSrc;
	// Use this for initialization
	void Start () {
        //player = GameObject.FindWithTag("Player").transform;
        if (river != null) {
            /*
            riverMesh = river.GetComponent<MeshFilter>().mesh;
            if (riverMesh == null) {
                Debug.Log("Rivermesh == null");
            }
            riverBounds = riverMesh.bounds;
            */
            riverBounds = river.GetComponent<Collider>();
            audioSrc = GetComponent<AudioSource>();
        }
        
	}

    // Update is called once per frame
    void Update() {


        transform.position = Physics.ClosestPoint(player.transform.position, riverBounds, river.transform.position, Quaternion.identity);
        //transform.position = player.transform.position;
        //river.transform.eulerAngles += new Vector3(0, 1, 0);



        float dist = Vector3.Distance(player.transform.position, transform.position);

        if (dist < 20f)
        {
            if (!audioSrc.isPlaying)
                audioSrc.Play();
        }
        else {
            audioSrc.Pause();
        }
       
        
	}
}
