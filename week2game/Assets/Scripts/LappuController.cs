﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LappuController : MonoBehaviour {

    public GameObject player;
    public GameObject lappu;

    public AudioSource source;
    public AudioClip openSound;
    public AudioClip closeSound;

	// Use this for initialization
	void Start () {
        source.clip = openSound;
        lappu.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKeyDown(KeyCode.M))
        {
            lappu.SetActive(false);
        }

        if (Input.GetKeyDown(KeyCode.Q)){

            if (lappu.activeInHierarchy)
            {
                source.clip = closeSound;
                source.Play();

                Debug.Log("Map closed");
                lappu.SetActive(false);
            }
            else if(!lappu.activeInHierarchy)
            {
                source.clip = openSound;
                source.Play();
                lappu.SetActive(true);
                Debug.Log("Map opened");
            }
        }

	}
}
