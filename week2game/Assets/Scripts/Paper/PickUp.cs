﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PickUp : MonoBehaviour {

    private bool grabObj = false;
    private GameObject hitObj;
    RaycastHit hit;
    private AudioSource Source;
    [SerializeField] private AudioClip discoverySound;

    List<GameObject> missingItems;
    public GameObject hammer;
    public GameObject nails;
    public GameObject planks;
    public MapController map;

    public GameObject hammerCheck;
    public GameObject nailsCheck;
    public GameObject planksCheck;

    public GameObject canvas;
    public Image panel;
    public Image panel2;
    public AudioListener listener;
    public GameObject endScreen;
    public GameObject player;
    public GameObject endText;

    public float timer;
    public bool gameStarted;

    // Use this for initialization
    void Start () {

        Source = GetComponent<AudioSource>();

        missingItems = new List<GameObject>();

        missingItems.Add(hammer);
        missingItems.Add(nails);
        missingItems.Add(planks);

        hammerCheck.SetActive(false);
        nailsCheck.SetActive(false);
        planksCheck.SetActive(false);

        gameStarted = false;
        panel2.gameObject.SetActive(false);

        //canvas.SetActive(false);

        timer = 0;

        
    }
	
	// Update is called once per frame
	void Update () {

        if (!gameStarted)
        {
            panel.CrossFadeAlpha(0, 5, false);
            gameStarted = true;
        }
    
        if (Physics.Raycast(transform.position + transform.forward, transform.forward, out hit, 3, ~8, QueryTriggerInteraction.Collide))
        {
            //PAPER
            if (hit.collider.gameObject.CompareTag("Collectible")) {
                Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * 1, Color.red);
                
                if (Input.GetKeyDown(KeyCode.E))
                {
                    hit.collider.gameObject.SetActive(false);
                }
            }

            //HAMMER
            if (hit.collider.gameObject.CompareTag("Hammer"))
            {
                Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * 1, Color.red);
                
                if (Input.GetKeyDown(KeyCode.E))
                {
                    Source.clip = discoverySound;
                    Source.Play();
                    hit.collider.gameObject.SetActive(false);
                    missingItems.Remove(hammer);
                    hammerCheck.SetActive(true);
                }
            }

            //NAILS
            if (hit.collider.gameObject.CompareTag("Nails"))
            {
                Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * 1, Color.red);
                Debug.Log("You are staring at nails.");

                if (Input.GetKeyDown(KeyCode.E))
                {
                    Source.clip = discoverySound;
                    Source.Play();
                    hit.collider.gameObject.SetActive(false);
                    missingItems.Remove(nails);
                    nailsCheck.SetActive(true);
                }
            }

            //PLANKS
            if (hit.collider.gameObject.CompareTag("Planks"))
            {
                Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * 1, Color.red);
                
                if (Input.GetKeyDown(KeyCode.E))
                {
                    Source.clip = discoverySound;
                    Source.Play();
                    hit.collider.gameObject.SetActive(false);
                    missingItems.Remove(planks);
                    planksCheck.SetActive(true);
                }
            }

            //BOAT
            if (hit.collider.gameObject.CompareTag("Boat"))
            {
                Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * 1, Color.red);
                Debug.Log("You are staring at a boat.");


                if (missingItems.Count.Equals(0))
                {
                    Debug.Log("Missing:" + missingItems.Count.ToString());

                    if (Input.GetKeyDown(KeyCode.E))
                    {
                        

                        canvas.SetActive(true);
                        panel.CrossFadeAlpha(15, 15, false);

                        timer = 7;
                        panel2.gameObject.SetActive(true);
                    }
                }
            }

            //MAP
            if (hit.collider.gameObject.CompareTag("Map"))
            {
                Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * 1, Color.red);
                Debug.Log("You are staring at map.");

                if (Input.GetKeyDown(KeyCode.E))
                {
                    Source.clip = discoverySound;
                    Source.Play();
                    map.SetFound();
                    hit.collider.gameObject.SetActive(false);
                }
            }

            

        }
        else
        {
            Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * 1000, Color.white);

            
        }

        Debug.Log((timer));

        if (timer > 0) {
                timer = timer - Time.deltaTime;
                if (timer < 5f) {
                    panel.CrossFadeAlpha(0, 5, false);
                    panel2.CrossFadeAlpha(255, 5, false);
                    listener.enabled = false;
                    endScreen.SetActive(true);
                    player.SetActive(false);
                    //endText.SetActive(true);
                }
            if (timer < 3f) {
                
            }
            }
    }
    
    

    }

