﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapController : MonoBehaviour {

    public GameObject player;
    public GameObject map;

    public AudioSource source;
    public AudioClip openSound;
    public AudioClip closeSound;

    public bool mapIsFound;

    public void SetFound() {
        mapIsFound = true;
    }

    // Use this for initialization
    void Start () {
        source.clip = openSound;
        map.SetActive(false);
        mapIsFound = false;
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Q))
        {
            map.SetActive(false);
        }

            if (mapIsFound)
        {
            Debug.Log("mAP IS fouNDED");
            if (Input.GetKeyDown(KeyCode.M))
            {

                if (map.activeInHierarchy)
                {
                    source.clip = closeSound;
                    source.Play();

                    Debug.Log("Map closed");
                    map.SetActive(false);
                }
                else if (!map.activeInHierarchy)
                {
                    map.SetActive(true);
                    source.clip = openSound;
                    source.Play();
                    
                    Debug.Log("Map opened");
                }
            }
        }
    }
}
